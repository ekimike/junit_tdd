package com.example.demojunittdd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoJunitTddApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoJunitTddApplication.class, args);
	}

}
