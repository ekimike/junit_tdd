Stack

- Spring Boot: 2.7.5
- Java 17
- Gradle
- Spring 5.3

Services:

* Product 
  * Controller/Api
  * Service
  * Repository
  * DB: H2 as runtime
* Review
  * Controller/Api
  * Service
  * Repository
  * MongoDB embedded 
* Inventory
  * Controller/Api
  * Service
    * Inventory Manager (External Service)

JUnit Architecture

Unit Test -> MockMVC -> Web Service -> Backend Resource

|_____________________________________________|

Resume testing

SQL back end
MongoDB backend
Third party API
